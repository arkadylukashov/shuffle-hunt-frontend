import createPersistedState from 'vuex-persistedstate'

export default ({ store, isHMR }) => {
  if (isHMR) return
  window.onNuxtReady(() => {
    createPersistedState({
      paths: ['themes', 'history', 'topics', 'filter', 'intervals', 'upvotes', 'badges']
    })(store)
  })
}
