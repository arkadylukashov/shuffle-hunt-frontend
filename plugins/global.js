import Vue from 'vue'

// Keyboard
import keyboardJS from 'keyboardjs'
Vue.prototype.$keyboard = keyboardJS

// Touch Events
import Vue2TouchEvents from 'vue2-touch-events'
Vue.use(Vue2TouchEvents)

// Tooltip
import VTooltip from 'v-tooltip'
Vue.use(VTooltip)

// Clipboard
import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)

// Confetti
import VueConfetti from 'vue-confetti'
Vue.use(VueConfetti)

// Analytics
import VueYandexMetrika from 'vue-yandex-metrika'
const METRIKA_ID = 54872593
Vue.use(VueYandexMetrika, {
  id: METRIKA_ID,
  env: 'production',
  options: {
    clickmap: true,
    trackLinks: true,
    trackHash: true,
    accurateTrackBounce: true,
    webvisor: true,
    triggerEvent: true,
  }
})

import VueAnalytics from 'vue-analytics'
const GOOGLE_ID = 'UA-45397442-2'
Vue.use(VueAnalytics, {
  id: GOOGLE_ID
})

Vue.prototype.$track = function(method, ...params) {
  let action = () => {
    // console.log("%cMetrika", "color:#1DC83F", method, ...params)
    this.$metrika[method](params)
  }
  if (this.$metrika) {
    action()
  } else {
    document.addEventListener(`yacounter${METRIKA_ID}inited`, () => {
      action()
    })
  }
  if (method == 'hit') {
    this.$ga.page('/')
  }
}
Vue.prototype.$goal = function(goal) {
  return this.$track('reachGoal', goal)
}
