import posts from './api/posts'
import topics from './api/topics'
import axios from 'axios'

let isDev = process.env.NODE_ENV == 'development'
axios.defaults.baseURL = isDev ? 'http://localhost:8000/' : 'https://api.shufflehunt.com/'
// axios.defaults.baseURL = 'https://api.shufflehunt.com/'

const api = {
  ...posts,
  ...topics,
}

export default api
