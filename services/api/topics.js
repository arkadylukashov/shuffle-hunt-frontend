import axios from 'axios'

export default {

  getTopics() {
    let url = `/topics/`
    return axios.get(url)
  }

}
