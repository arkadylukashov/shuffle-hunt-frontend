import axios from 'axios'
import queryString from 'query-string'
import md5 from 'md5'

const filterize = function(filters, counter) {
  let query = {}
  if (!!filters.topics) {
    query.topics = filters.topics.join(',')
    query.contains = filters.contains
  }
  if (!!filters.dates) {
    query.since = filters.dates[0]
    query.until = filters.dates[1]
  }
  if (!!filters.upvotes) {
    query.upvotes = filters.upvotes
  }
  if (!!filters.badges) {
    query.badges = filters.badges
  }
  if (!!counter) {
    query.hash = md5(counter)
  }
  if (process.env.NODE_ENV == 'development') {
    query.__godmode = 1
  }
  return queryString.stringify(query)
}

export default {

  getRandomPost(filters, counter, history, promo) {
    let queryParams = '?' + filterize(filters, counter)
    let url = `/posts/random/${queryParams}`
    return axios({
      method: 'get',
      url: url,
      headers: {
        'X-Requested': this.generateHeader(counter),
        'X-Time': this.generateHeader(history),
        'X-Content': this.generateHeader(promo),
      }
    })
  },

  generateHeader(content) {
    let prefix = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 3);
    let result = btoa(content).replace(/==/, '.').replace(/=/, ';');
    return `${prefix}_${result}`
  }

}
