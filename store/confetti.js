export const state = () => ({
  show: false,
  message: null,
})

export const getters = {

  isShow(state, getters) {
    return state.show
  },

  message(state, getters) {
    return state.message
  },

}

export const mutations = {

  TOGGLE_SHOW(state, payload) {
    state.show = payload
  },

  SET_MESSAGE(state, payload) {
    state.message = payload
  },

}

export const actions = {

  show({ commit, state }, { duration = 3000, message = null } = {}) {
    commit('SET_MESSAGE', message)
    if (!state.show) {
      commit('TOGGLE_SHOW', true)
      setTimeout(() => {
        commit('TOGGLE_SHOW', false)
      }, duration)
    }
  },

}
