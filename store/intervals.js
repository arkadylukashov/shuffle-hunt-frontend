import moment from 'moment'
import { cloneDeep } from 'lodash'

export const state = () => ({
  items: [
    {
      selected: false,
      code: 'week',
      title: 'Last week',
      params: {
        count: 7,
        value: 'days',
      },
    },
    {
      selected: false,
      code: '2weeks',
      title: 'Last 2 weeks',
      params: {
        count: 14,
        value: 'days',
      },
    },
    {
      selected: false,
      code: 'month',
      title: 'Last month',
      params: {
        count: 1,
        value: 'month',
      },
    },
    {
      selected: false,
      code: '3months',
      title: 'Last 3 months',
      params: {
        count: 3,
        value: 'month',
      },
    },
    {
      selected: false,
      code: '6months',
      title: 'Last 6 months',
      params: {
        count: 6,
        value: 'month',
      },
    },
    {
      selected: false,
      code: 'year',
      title: 'Last year',
      params: {
        count: 1,
        value: 'year',
      },
    },
    {
      selected: false,
      code: '2years',
      title: 'Last 2 years',
      params: {
        count: 2,
        value: 'year',
      },
    },
    {
      selected: false,
      code: '3years',
      title: 'Last 3 years',
      params: {
        count: 3,
        value: 'year',
      },
    },
  ],
})

export const getters = {

  list(state) {
    let items = cloneDeep(state.items)
    items.forEach((item) => {
      item.range = [
        moment().subtract(item.params.count, item.params.value).format('YYYY-MM-DD'),
        moment().format('YYYY-MM-DD')
      ]
    })
    return items
  },

  codes(state, getters) {
    return state.items.map(item => item.code)
  },

  active(state, getters) {
    let item = getters.list.find(item => item.selected == true)
    return !!item ? item : null
  },

}

export const mutations = {

  SET_ACTIVE(state, payload) {
    let interval = state.items.find(item => item.code == payload)
    interval.selected = true
  },

  RESET(state) {
    state.items.forEach((item) => {
      item.selected = false
    })
  },

}

export const actions = {

  reset({ commit }) {
    commit('RESET')
  },

  setActive({ commit }, payload) {
    this._vm.$goal('use_filter_dates')
    commit('RESET')
    commit('SET_ACTIVE', payload)
  },

}
