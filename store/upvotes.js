export const state = () => ({
  items: [
    {
      title: '50+',
      value: 50,
      selected: false,
    },
    {
      title: '100+',
      value: 100,
      selected: false,
    },
    {
      title: '200+',
      value: 200,
      selected: false,
    },
    {
      title: '300+',
      value: 300,
      selected: false,
    },
    {
      title: '400+',
      value: 400,
      selected: false,
    },
    {
      title: '500+',
      value: 500,
      selected: false,
    },
    {
      title: '600+',
      value: 600,
      selected: false,
    },
    {
      title: '700+',
      value: 700,
      selected: false,
    },
    {
      title: '800+',
      value: 800,
      selected: false,
    },
    {
      title: '900+',
      value: 900,
      selected: false,
    },
  ],
})

export const getters = {

  list(state) {
    return state.items
  },

  active(state, getters) {
    let item = getters.list.find(item => item.selected == true)
    return !!item ? item : null
  },

}

export const mutations = {

  SET_ACTIVE(state, payload) {
    let upvote = state.items.find(item => item.value == payload.value)
    upvote.selected = true
  },

  RESET(state) {
    state.items.forEach((item) => {
      item.selected = false
    })
  },

}

export const actions = {

  reset({ commit }) {
    commit('RESET')
  },

  setActive({ commit }, payload) {
    commit('RESET')
    commit('SET_ACTIVE', payload)
  },

}
