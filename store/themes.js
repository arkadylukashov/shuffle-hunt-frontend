export const state = () => ({
  items: [
    {
      code: "dark",
      active: true,
      color: "#0C2134",
    },
    {
      code: "light",
      active: false,
      color: "#F5F7F9",
    },
    {
      code: "red",
      active: false,
      color: ["#CA395B", "#DC615B"],
    },
    {
      code: "blue",
      active: false,
      radial: true,
      color: ["#4C7CB7", "#26669C"],
    },
    {
      code: "green",
      active: false,
      radial: true,
      color: ["#1ABFA1", "#0F9A8D"],
    },
  ]
})

export const getters = {

  list(state) {
    return state.items
  },

  current(state) {
    return state.items.find(item => item.active == true)
  },

  style(state, getters) {
    let background
    if (typeof getters.current.color == 'string') {
      background = getters.current.color
    } else {
      if (getters.current.radial) {
        background = `
          radial-gradient(760px at 50.7% 60.5%, ${getters.current.color[0]} 0%, ${getters.current.color[1]} 90%)
        `
      } else {
        background = `
        linear-gradient(to right, ${getters.current.color[0]}, ${getters.current.color[1]})
      `
      }
    }
    return {
      background
    }
  },

}

export const mutations = {

  SET_THEME(state, payload) {
    state.items.forEach((item) => {
      item.active = item.code == payload ? true : false
    })
  },

}

export const actions = {

  switchTheme({ commit }, code) {
    this._vm.$goal('switch_theme')
    commit('SET_THEME', code)
  },

}
