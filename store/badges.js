export const state = () => ({
  items: [
    {
      title: 'Any',
      value: 'any',
      selected: false,
    },
    {
      title: 'Daily badge',
      value: 'daily',
      selected: false,
    },
    {
      title: 'Weekly badge',
      value: 'weekly',
      selected: false,
    },
    {
      title: 'Monthly badge',
      value: 'monthly',
      selected: false,
    },
  ]
})

export const getters = {

  list(state) {
    return state.items
  },

  active(state, getters) {
    let item = getters.list.find(item => item.selected == true)
    return !!item ? item : null
  },

}

export const mutations = {

  SET_ACTIVE(state, payload) {
    let badge = state.items.find(item => item.value == payload.value)
    badge.selected = true
  },

  RESET(state) {
    state.items.forEach((item) => {
      item.selected = false
    })
  },

}

export const actions = {

  reset({ commit }) {
    commit('RESET')
  },

  setActive({ commit }, payload) {
    commit('RESET')
    commit('SET_ACTIVE', payload)
  },

}
