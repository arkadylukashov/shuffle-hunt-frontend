import api from '~/services/api.js'

export const state = () => ({
  isLoading: false,
  item: null
})

export const getters = {

  isLoading(state) {
    return state.isLoading
  },

  filters(state, getters, rootState, rootGetters) {
    return rootGetters['filter/all']
  },

  counter(state, getters, rootState) {
    return rootState.history.counter
  },

}

export const mutations = {

  SET_PRODUCT_DATA(state, payload) {
    state.item = payload
  },

  NO_PRODUCT(state) {
    state.item = null
  },

  LOADING_START(state) {
    state.isLoading = true
  },

  LOADING_FINISH(state) {
    state.isLoading = false
  },

}

export const actions = {

  random({ commit, getters, rootGetters }, params) {
    this._vm.$goal('shuffle')
    commit('LOADING_START')
    return api.getRandomPost(
      getters.filters,
      getters.counter,
      rootGetters['history/lastIds'],
      rootGetters['history/promoIds']
    ).then((response) => {
      commit('LOADING_FINISH')
      let result = response.data
      if (result.status == 200) {
        commit('SET_PRODUCT_DATA', result.body)
      } else {
        commit('NO_PRODUCT')
        if (response.data && response.data.status == 404) {
          return Promise.reject('empty')
        } else {
          return Promise.reject('server')
        }
      }
    })
  },

}
