export const state = () => ({
  counter: 0,
  limit: 24,
  list: [],
  ids: [],
})

export const getters = {

  items(state) {
    return state.list
  },

  canViewHistory(state) {
    return state.list.length > 0
  },

  lastIds(state) {
    return state.list.length > 0 ? _.take(_.map(state.list, 'id'), 5) : []
  },

  promoIds(state) {
    return state.ids
  },

  count(state) {
    return state.counter
  },
}

export const mutations = {

  ADD(state, payload) {
    let product = {
      id: payload.Id,
      title: payload.Title,
      tagline: payload.Tagline,
      image: payload.Image,
      upvotes: payload.UpvotesCount,
      url: payload.ProducthuntUrl,
    }
    state.list.unshift(product)
  },

  ADD_PROMO(state, payload) {
    state.ids.push(payload.Id)
  },

  RESET_PROMO(state) {
    state.ids = []
  },

  INCREASE_COUNTER(state) {
    state.counter += 1
  },

  REMOVE_LAST(state) {
    let index = state.limit - 1
    state.list.splice(state.list.indexOf(index), 1)
  },

}

export const actions = {

  push({ commit, state }, product) {
    let itemExists = state.list.find(item => item.id == product.Id)
    if (!itemExists) {
      commit('INCREASE_COUNTER')
      let count = state.list.length + 1
      if (count > state.limit) {
        commit('REMOVE_LAST')
      }
      commit('ADD', product)
      let goals = [10, 50, 100, 250, 500, 1000]
      if (goals.includes(state.counter)) {
        if (state.counter >= 100) {
          this.dispatch('confetti/show', {
            message: `Wow! You've just shuffled <strong>${state.counter}</strong>th product!`,
            duration: 4000
          })
        }
        this._vm.$goal(`shuffle_counter_${state.counter}`)
      }
    }
  },

  pushToPromo({ commit }, product) {
    commit('ADD_PROMO', product)
  },

  resetPromo({ commit }) {
    commit('RESET_PROMO')
  },

}
