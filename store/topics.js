import api from "~/services/api.js"

export const state = () => ({
  isLoading: false,
  searchQuery: '',
  items: [],
})

export const getters = {

  list(state) {
    return state.items
  },

  query(state) {
    return state.searchQuery
  },

}


export const mutations = {

  SET_TOPICS(state, payload) {
    state.items = payload
  },

  LOADING_START(state) {
    state.isLoading = true
  },

  LOADING_FINISH(state) {
    state.isLoading = false
  },

  SET_QUERY(state, payload) {
    state.searchQuery = payload
  },

}



export const actions = {

  load({ commit, getters }) {
    setTimeout(() => {
      if (getters.list.length == 0) {
        api.getTopics().then((response) => {
          commit('LOADING_FINISH')
          let result = response.data
          if (result.status == 200) {
            commit('SET_TOPICS', result.body)
          }
        })
      }
    }, 1)
  },

  setSearchQuery({ commit }, payload) {
    commit('SET_QUERY', payload)
  },

  resetSearchQuery({ commit }) {
    commit('SET_QUERY', '')
  },

}
