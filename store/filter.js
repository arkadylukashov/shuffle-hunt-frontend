import { forEach, sampleSize, sample } from 'lodash'

export const state = () => ({
  used: false,
  inProgress: false,
  _contains: 'some',
  _badges: null,
  _topics: [],
  _dates: [],
  _upvotes: 0,
})

export const getters = {

  isUsed(state) {
    return state.used
  },

  topics(state) {
    return state._topics.length ? state._topics : null
  },

  topicsContains(state) {
    return state._contains
  },

  dates(state, getters, rootState, rootGetters) {
    let activeInterval = rootGetters['intervals/active']
    if (!!activeInterval) {
      return activeInterval.range
    } else {
      return state._dates.length ? state._dates : null
    }
  },

  upvotes(state) {
    return state._upvotes > 0 ? state._upvotes : null
  },

  badges(state) {
    return state._badges
  },

  any(state, getters) {
    return Object.keys(getters.all).length > 0
  },

  all(state, getters) {
    let result = {}
    if (!!getters.topics) {
      result.topics = state._topics
      result.contains = state._contains
    }
    if (!!getters.dates) {
      result.dates = getters.dates
    }
    if (!!getters.upvotes) {
      result.upvotes = state._upvotes
    }
    if (!!getters.badges) {
      result.badges = state._badges
    }
    return result
  },

  scheme(state, getters, rootState, rootGetters) {
    let result = []
    forEach(getters.all, (item, index) => {
      let res = typeof item == 'Array' ? item.join(',') : item
      if (index == 'dates' && rootGetters['intervals/active']) {
        res = rootGetters['intervals/active'].code
      }
      if (index == 'contains' && res == 'some') {
        return
      }
      result.push(`${index}:${res}`)
    })
    return result.join(';')
  },

  share(state, getters) {
    return `https://shufflehunt.com/#filter=${getters.scheme}`
  }

}


export const mutations = {

  SET_USED(state) {
    state.used = true
  },

  PROGRESS_START(state) {
    state.inProgress = true
  },

  PROGRESS_FINISH(state) {
    state.inProgress = false
  },

  ADD_TOPIC(state, payload) {
    state._topics.push(payload)
  },

  REMOVE_TOPIC(state, payload) {
    state._topics.splice(state._topics.indexOf(payload), 1)
  },

  RESET_TOPICS(state) {
    state._topics = []
    state._contains = 'some'
  },


  SET_CONTAINS(state, payload) {
    state._contains = payload
  },


  SET_DATES(state, payload) {
    state._dates = payload
  },

  RESET_DATES(state) {
    state._dates = []
  },


  SET_UPVOTES(state, payload) {
    state._upvotes = payload
  },

  RESET_UPVOTES(state) {
    state._upvotes = 0
  },


  SET_BADGES(state, payload) {
    state._badges = payload
  },

  RESET_BADGES(state) {
    state._badges = null
  },

}



export const actions = {

  selectTopic({ commit, state }, payload) {
    if (!state.used) {
      commit('SET_USED')
    }
    if (!state.inProgress) {
      this._vm.$goal('use_filter_topics')
    }
    commit('ADD_TOPIC', payload)
  },

  unSelectTopic({commit}, payload) {
    commit('REMOVE_TOPIC', payload)
  },

  resetTopics({ commit }) {
    commit('RESET_TOPICS')
    commit('SET_CONTAINS', 'some')
  },

  setContains({ commit, state }, payload) {
    if (!state.inProgress) {
      this._vm.$goal('use_filter_contains')
    }
    commit('SET_CONTAINS', payload)
  },

  setDates({ commit, state }, payload) {
    if (!state.used) {
      commit('SET_USED')
    }
    commit('SET_DATES', payload)
  },

  resetDates({ commit }) {
    this.dispatch('intervals/reset')
    commit('RESET_DATES')
  },

  setUpvotes({ commit, state }, payload) {
    if (!state.used) {
      commit('SET_USED')
    }
    if (!state.inProgress) {
      this._vm.$goal('use_filter_upvotes')
    }
    this.dispatch('upvotes/setActive', payload)
    commit('SET_UPVOTES', payload.value)
  },

  resetUpvotes({ commit }) {
    this.dispatch('upvotes/reset')
    commit('RESET_UPVOTES')
  },

  setBadges({ commit, state }, payload) {
    if (!state.used) {
      commit('SET_USED')
    }
    if (!state.inProgress) {
      this._vm.$goal('use_filter_badges')
    }
    this.dispatch('badges/setActive', payload)
    commit('SET_BADGES', payload.value)
  },

  resetBadges({ commit }) {
    this.dispatch('badges/reset')
    commit('RESET_BADGES')
  },

  resetAll({ dispatch }) {
    dispatch('resetTopics')
    dispatch('resetDates')
    dispatch('resetUpvotes')
    dispatch('resetBadges')
  },

  fetchFromUrl({ dispatch }) {
    let hash = window.location.hash
    if (hash) {
      let match = hash.match(/^#filter=(.*)/)
      if (match) {
        let lines = match[1].split(';')
        let data = {}
        lines.forEach((item) => {
          let param = item.split(':')
          let code = param[0]
          let value = param[1]
          if (code == 'topics') {
            value = param[1].split(',').map((val) => {
              return parseInt(val)
            })
          }
          if (code == 'upvotes') {
            value = parseInt(value)
          }
          data[code] = value
        })
        dispatch('setFilterByData', data)
      }
    }
  },

  setFilterByData({ commit, dispatch, rootGetters }, { topics, contains, dates, upvotes, badges }) {
    commit('PROGRESS_START')
    dispatch('resetAll')
    /** [topics] */
    if (topics && topics.length) {
      topics.forEach((topic) => {
        if (rootGetters['topics/list'].find(item => item.Id == topic)) {
          dispatch('selectTopic', topic)
        }
      })
    }
    /** [topics contains] */
    if (contains) {
      if (['some', 'all'].includes(contains)) {
        dispatch('setContains', contains)
      }
    }
    /** [upvotes] */
    if (upvotes) {
      let upvotesObject = rootGetters['upvotes/list'].find(item => item.value == upvotes)
      if (upvotesObject) {
        dispatch('setUpvotes', upvotesObject)
      }
    }
    /** [badges] */
    if (badges) {
      let badgesObject = rootGetters['badges/list'].find(item => item.value == badges)
      if (badgesObject) {
        dispatch('setBadges', badgesObject)
      }
    }
    /** [dates] */
    if (dates) {
      if (rootGetters['intervals/codes'].includes(dates)) {
        this.dispatch('intervals/setActive', dates)
      }
    }
    commit('PROGRESS_FINISH')
  },

  setRandomFilters({ dispatch, rootGetters }) {
    this._vm.$goal('randomize_usage')
    dispatch('resetAll')
    let result = {}
    /** [topics] */
    let topics = sampleSize(rootGetters['topics/list'].slice(0, 40), sample([0, 0, 1, 1, 1, 2, 2, 3, 4]))
    if (topics.length) {
      result.topics = topics.map(topic => topic.Id)
    }
    /** [topics contains] */
    if (sample([0, 1, 2]) == 1) {
      result.contains = 'all'
    }
    /** [upvotes] */
    if (sample([0, 1]) == 0) {
      result.dates = sample(rootGetters['intervals/codes'])
    }
    /** [badges] */
    if (sample([0, 1]) == 0) {
      let upvotes = sample(rootGetters['upvotes/list'])
      result.upvotes = upvotes.value
    }
    /** [dates] */
    if (sample([0, 1, 2]) == 2) {
      let badges = sample(rootGetters['badges/list'])
      result.badges = badges.value
    }
    dispatch('setFilterByData', result)
  },

}
