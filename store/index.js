export const state = () => ({
  started: false,
  isRandomBlock: false,
  isShowHistory: false,
  isShowFilter: false,
  isDistractionFree: false,
  totalCount: 0,
  activeFilterWidget: false
})


export const mutations = {

  START(state) {
    state.started = true
  },

  BLOCK_SHUFFLING(state, payload) {
    state.isRandomBlock = payload
  },

  TOGGLE_HISTORY(state, payload) {
    state.isShowHistory = payload
  },

  TOGGLE_FILTER(state, payload) {
    state.isShowFilter = payload
  },

  SET_TOTAL_COUNT(state, payload) {
    state.totalCount = payload
  },

  TOGGLE_DISTRACTION_FREE(state, payload) {
    state.isDistractionFree = payload
  },

  SET_ACTIVE_FILTER_WIDGET(state, payload) {
    state.activeFilterWidget = payload
  },

  TOGGLE_CONFETTI(state, payload) {
    state.isConfettiShow = payload
  },

}


export const getters = {

  isStarted(state) {
    return state.started
  },

  activeFilterWidget(state) {
    return state.activeFilterWidget
  },

  isShowCards(state, getters, rootState, rootGetters) {
    if (rootGetters['history/canViewHistory']) {
      return true
    } else {
      if (getters.isStarted) {
        return true
      } else {
        return false
      }
    }
  },

  isShowIosPopup() {
    return !(window.navigator.standalone === true)
  },

}


export const actions = {

  start({ commit }) {
    commit('START')
  },

  showHistory({ commit, state }) {
    commit('TOGGLE_HISTORY', true)
    commit('BLOCK_SHUFFLING', true)
  },

  closeHistory({ commit, state }) {
    commit('TOGGLE_HISTORY', false)
    commit('BLOCK_SHUFFLING', false)
  },

  showFilter({ commit, state }) {
    commit('TOGGLE_FILTER', true)
    commit('BLOCK_SHUFFLING', true)
  },

  toggleFilter({ commit, state, dispatch }) {
    if (state.isShowFilter) {
      dispatch('closeFilter')
    } else {
      dispatch('openFilter')
    }
  },

  openFilter({ commit, dispatch }) {
    dispatch('topics/resetSearchQuery')
    commit('TOGGLE_FILTER', true)
    commit('BLOCK_SHUFFLING', true)
  },

  closeFilter({ commit }) {
    commit('TOGGLE_FILTER', false)
    commit('BLOCK_SHUFFLING', false)
  },

  blockShuffling({ commit }, payload) {
    commit('BLOCK_SHUFFLING', payload)
  },

  setProductsTotalCount({ commit }, payload) {
    commit('SET_TOTAL_COUNT', payload)
  },

  toggleDistractionFree({ commit, state }) {
    let value = !state.isDistractionFree
    commit('TOGGLE_DISTRACTION_FREE', value)
  },

  setActiveFilterWidget({ commit }, payload) {
    commit('SET_ACTIVE_FILTER_WIDGET', payload)
  },

}
