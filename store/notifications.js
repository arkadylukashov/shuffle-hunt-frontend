export const state = () => ({
  show: false,
  message: '',
})

export const getters = {

  isShow(state, getters) {
    return state.show
  },

  message(state, getters) {
    return state.message
  },

}

export const mutations = {

  TOGGLE_SHOW(state, payload) {
    state.show = payload
  },

  SET_MESSAGE(state, payload) {
    state.message = payload
  },

}

export const actions = {

  show({ commit, state }, payload) {
    commit('SET_MESSAGE', payload)
    commit('TOGGLE_SHOW', true)
    setTimeout(() => {
      commit('TOGGLE_SHOW', false)
    }, 1000)
  },

}
