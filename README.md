# Shuffle Hunt [Frontend]

The brand new way to discover products on Product Hunt in one touch. Build on Nuxt.js & Vue.js

Production: [shufflehunt.com](https://shufflehunt.com)

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:10000
$ yarn dev -o

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn generate
```

## Deploy

I want to set up automatic deploy by Bitbucket webhooks, but now only this hacky way

``` bash
$ yarn deploy
```

## TODO

- [ ] Test todo item
- [x] Checked ite
