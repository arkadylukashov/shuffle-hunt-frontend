const shell = require('shelljs')
const chalk = require('chalk')
const fs = require('fs')
const log = console.log
const program = require('commander')

program
  .option('-p, --production', 'deploy to production')
  .option('-s, --staging', 'deploy to staging')
  .parse(process.argv)

let config
try {
  config = require('./server-config.json')
} catch(ex) {
  log(chalk.white.bgRed(' No config present '))
  return
}

let mode
if (program.production && !!config.production) {
  log(chalk.white.bgMagenta(' Deploying to PRODUCTION '))
  mode = 'production'
} else {
  log(chalk.white.bgBlue(' Deploying to STAGING '))
  mode = 'staging'
}

const getExecString = () => {
  let data = config[mode]
  if (data.pass) {
    return `rsync -az --force --delete --progress -e "sshpass -p ${data.pass} ssh -p22" ./dist/ ${data.user}@${data.ip}:${data.dir}`
  } else {
    return `rsync -az --force --delete --progress -e "ssh -p22" ./dist/ ${data.user}@${data.ip}:${data.dir}`
  }
}


log(chalk.green('---------------'))
log(chalk.green('Build frontend'))
log(chalk.green('---------------'))
shell.exec('yarn generate')
log(chalk.green('---------------'))
log(chalk.green('Upload to server'))
log(chalk.green('---------------'))
shell.exec(getExecString())
log(chalk.black.bgGreen(' OK '))
log(chalk.black.bgGreen(` Go to https://${config[mode].host} `))
