module.exports = {

  render: { resourceHints: false },

  head: {
    title: "ShuffleHunt – Discover random products from Product Hunt",
    meta: [
      { charset: 'utf-8' },
      { hid: 'description', name: 'description', content: 'The brand new way to discover products on Product Hunt in one touch' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=no' },
      // iOS standalone
      { name: 'mobile-web-app-capable', content: 'yes' },
      { name: 'apple-mobile-web-app-capable', content: 'yes' },
      { name: 'apple-mobile-web-app-title', content: 'ShuffleHunt' },
      { name: 'apple-mobile-web-app-status-bar-style', content: 'black-translucent' },
      // SEO verification
      { name: 'yandex-verification', content: '094a8fcf3efdc2be' },
      { name: 'google-site-verification', content: 'pw3VH04niifhKFuhrYADCA0OFZMl2tBlE-TA6OOIpUI' },
      // Twitter
      { hid: 'twitter:card', name: 'twitter:card', content: 'summary_large_image' },
      { hid: 'twitter:site', name: 'twitter:site', content: 'https://shufflehunt.com' },
      { hid: 'twitter:creator', name: 'twitter:creator', content: '@arkadylukashov' },
      { hid: 'twitter:title', name: 'twitter:title', content: 'Shufflehunt' },
      { hid: 'twitter:description', name: 'twitter:description', content: 'The brand new way to discover products on Product Hunt in one touch' },
      { hid: 'twitter:image', name: 'twitter:image', content: 'https://shufflehunt.com/share.jpg' },
      { hid: 'og:url', name: 'og:url', content: 'https://shufflehunt.com' },
      { hid: 'og:type', name: 'og:type', content: "website" },
      { hid: 'og:title', name: 'og:title', content: 'Shufflehunt' },
      { hid: 'og:description', name: 'og:description', content: 'The brand new way to discover products on Product Hunt in one touch' },
      { hid: 'og:image', name: 'og:image', content: 'https://shufflehunt.com/share.jpg' },
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/favicon.png' },
      // Chrome standalone
      { rel: 'manifest', href: '/manifest.json' },
      // iOS standalone
      { rel: 'apple-touch-icon', sizes: "152x152", href: '/standalone/ios152x152.png' },
      { rel: 'apple-touch-icon', sizes: "180x180", href: '/standalone/ios180x180.png' },
      { rel: 'apple-touch-icon', sizes: "167x167", href: '/standalone/ios167x167.png' },
      { rel: 'apple-touch-startup-image', href: '/standalone/ios-startup.png' },
    ],
  },

  generate: {
    minify: {
      collapseWhitespace: true,
      removeComments: true,
      removeTagWhitespace: true,
      sortClassName: true,
    }
  },

  loading: false,

  css: [
    { src: '~/assets/styles/base.scss', lang: 'scss' },
  ],

  plugins: [
    { src: '~plugins/global', ssr: false },
    { src: '~plugins/localStorage', ssr: false },
  ],

  modules: [
    [
      '@nuxtjs/style-resources',
      'nuxt-svg-loader',
      '@nuxtjs/sitemap',
    ]
  ],

  sitemap: {
    generate: true,
    gzip: true,
    hostname: 'https://shufflehunt.com',
  },

  styleResources: {
    scss: [
      './assets/styles/variables.scss',
      './assets/styles/mixins.scss'
    ]
  },

  build: {

    postcss: {
      plugins: {
        'autoprefixer': {},
        'postcss-base64': {
          extensions: ['.svg', '.png', '.jpg']
        },
      },
      preset: {
        autoprefixer: {
          grid: true
        }
      }
    },
    extend(config, { isDev, isClient }) {
      config.node = {
        fs: 'empty'
      }
      if (isDev && process.client) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
        if (!isDev) {
          config.output.publicPath = './_nuxt/'
        }
        return config;
      }
    }
  }
}
